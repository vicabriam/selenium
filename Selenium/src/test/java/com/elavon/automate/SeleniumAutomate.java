package com.elavon.automate;

import java.util.concurrent.TimeUnit;

import io.github.bonigarcia.wdm.Architecture;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumAutomate {
    WebDriver driver;

    public WebDriver setupWebDriver(String browser) {
        if (browser == "firefox") {
            FirefoxDriverManager.getInstance().architecture(Architecture.x32).version(".18").setup();
            System.out.println("Debug 2");
            System.out.println("Debug 3");
            driver = new FirefoxDriver();
            System.out.println("Debug 4");
        }
        else if (browser == "chrome") {
            ChromeDriverManager.getInstance().setup();
            driver = new ChromeDriver();
        }
        else if (browser == "iexplorer") {
            InternetExplorerDriverManager.getInstance().setup();
            DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
            ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
            driver = new InternetExplorerDriver();
        }
        else {
            InternetExplorerDriverManager.getInstance().setup();
            DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
            ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
            driver = new InternetExplorerDriver();
        }
        return driver;
    }

    @Before
    public void setupTest() {
        driver = setupWebDriver("chrome");
    }

    @Ignore
    @Test
    public void TestDemo() {
        String baseUrl = "http://demoqa.com/";
        driver.navigate().to(baseUrl);
        WebElement registration = driver.findElement(By.id("menu-item-374"));
        registration.click();
        WebElement firstName = driver.findElement(By.name("first_name"));
        WebDriverWait myWaitVar = new WebDriverWait(driver, 5);
        myWaitVar.until(ExpectedConditions.visibilityOf(firstName));
        firstName.sendKeys("Lodi");
        WebElement lastName = driver.findElement(By.id("name_3_lastname"));
        lastName.sendKeys("Werpa");
        WebElement status = driver.findElement(By.cssSelector("[name='radio_4[]'][value='single']"));
        status.click();
        WebElement hobby1 = driver.findElement(By.cssSelector("[name='checkbox_5[]'][value='reading']"));
        hobby1.click();
        WebElement hobby2 = driver.findElement(By.cssSelector("[name='checkbox_5[]'][value='cricket ']"));
        hobby2.click();
        Select countryDropdown = new Select(driver.findElement(By.id("dropdown_7")));
        countryDropdown.selectByVisibleText("Philippines");
        Select monthDropdown = new Select(driver.findElement(By.id("mm_date_8")));
        monthDropdown.selectByVisibleText("1");
        Select dayDropdown = new Select(driver.findElement(By.id("dd_date_8")));
        dayDropdown.selectByVisibleText("1");
        Select yearDropdown = new Select(driver.findElement(By.id("yy_date_8")));
        yearDropdown.selectByVisibleText("2014");
        WebElement phoneNum = driver.findElement(By.id("phone_9"));
        phoneNum.sendKeys("09696969696");
        WebElement username = driver.findElement(By.id("username"));
        username.sendKeys("lodiwerpa");
        WebElement email = driver.findElement(By.id("email_1"));
        email.sendKeys("lodiwerpa@payaman.com");
        driver.findElement(By.id("profile_pic_10")).sendKeys("C:/Users/VCAbriam/Desktop/se_logo.jpg");
        WebElement about = driver.findElement(By.id("description"));
        about.sendKeys("Pawer!!!");
        WebElement password = driver.findElement(By.id("password_2"));
        password.sendKeys("petmaluwerpa69");
        WebElement confirm = driver.findElement(By.id("confirm_password_password_2"));
        confirm.sendKeys("petmaluwerpa69");
        WebElement submit = driver.findElement(By.name("pie_submit"));
        submit.click();
    }

    @Test
    public void TestDemo2() {
        String baseUrl = "http://demoqa.com/";
        driver.navigate().to(baseUrl);
        try {
            TimeUnit.SECONDS.sleep(15);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        WebElement registration = driver.findElement(By.id("menu-item-374"));
        registration.click();
        
        System.out.println(driver.getTitle());
        
        WebElement firstName = driver.findElement(By.name("first_name"));
        WebDriverWait myWaitVar = new WebDriverWait(driver, 5);
        myWaitVar.until(ExpectedConditions.visibilityOf(firstName));
        firstName.sendKeys("Lodi");
        WebElement lastName = driver.findElement(By.id("name_3_lastname"));
        lastName.sendKeys("Werpa");
        WebElement status = driver.findElement(By.cssSelector("[name='radio_4[]'][value='single']"));
        status.click();
        WebElement hobby1 = driver.findElement(By.cssSelector("[name='checkbox_5[]'][value='reading']"));
        hobby1.click();
        WebElement hobby2 = driver.findElement(By.cssSelector("[name='checkbox_5[]'][value='cricket ']"));
        hobby2.click();
        Select countryDropdown = new Select(driver.findElement(By.id("dropdown_7")));
        countryDropdown.selectByVisibleText("Philippines");
        Select monthDropdown = new Select(driver.findElement(By.id("mm_date_8")));
        monthDropdown.selectByVisibleText("1");
        Select dayDropdown = new Select(driver.findElement(By.id("dd_date_8")));
        dayDropdown.selectByVisibleText("1");
        Select yearDropdown = new Select(driver.findElement(By.id("yy_date_8")));
        yearDropdown.selectByVisibleText("2014");
        WebElement phoneNum = driver.findElement(By.id("phone_9"));
        phoneNum.sendKeys("09696969696");
        WebElement username = driver.findElement(By.id("username"));
        username.sendKeys("lodiwerpa");
        WebElement email = driver.findElement(By.id("email_1"));
        email.sendKeys("lodiwerpa@payaman.com");
        driver.findElement(By.id("profile_pic_10")).sendKeys("C:/Users/VCAbriam/Desktop/se_logo.jpg");
        WebElement about = driver.findElement(By.id("description"));
        about.sendKeys("Pawer!!!");
        WebElement password = driver.findElement(By.id("password_2"));
        password.sendKeys("petmaluwerpa69");
        WebElement confirm = driver.findElement(By.id("confirm_password_password_2"));
        confirm.sendKeys("petmaluwerpa69");
        WebElement submit = driver.findElement(By.name("pie_submit"));
        submit.click();
    }
}
